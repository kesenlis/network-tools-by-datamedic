package com.datamedic.networktools.response;

public interface HostAsyncResponse extends LanHostAsyncResponse, WanHostAsyncResponse, ErrorAsyncResponse {
}
