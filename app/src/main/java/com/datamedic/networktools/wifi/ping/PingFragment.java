/*
 * Network Tools
 * Copyright (C) 2018  DataMedic Business IT Support <networktools@datamedic.gr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.datamedic.networktools.wifi.ping;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.datamedic.networktools.MainContext;
import com.datamedic.networktools.R;
import com.datamedic.networktools.navigation.NavigationMenu;
import com.datamedic.networktools.wifi.ping.ping.PingResult;
import com.datamedic.networktools.wifi.ping.ping.PingStats;

import java.net.InetAddress;
import java.util.Objects;



public class PingFragment extends Fragment {


    private Button pingButton, updateipButton;
    private EditText editIpAddress;
    public boolean PingBooleanButton;
    private TextView errorTextview, pingresultTextView;
    private ProgressDialog ProgressDialog;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pingtool, container, false);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        editIpAddress = view.findViewById(R.id.editIpAddress);
        updateipButton = view.findViewById(R.id.updateipButton);
        pingButton = view.findViewById(R.id.pingButton);
        errorTextview = view.findViewById(R.id.errorTextview);
        pingresultTextView = view.findViewById(R.id.pingresultTextView);

        InetAddress ipAddress = IPTools.getLocalIPv4Address();
        if (ipAddress != null) {
            editIpAddress.setText(ipAddress.getHostAddress());
        } else errorTextview.setText("Please Connect to Network");


        updateipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                InetAddress ipAddress = IPTools.getLocalIPv4Address();
                if (ipAddress != null) {
                    editIpAddress.setText(ipAddress.getHostAddress());
                } else errorTextview.setText("Please Connect to Network with valid IPV4 Address");

            }
        });


        pingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                new Thread(new Runnable() {
                    @Override
                    public void run() {


                        try {
                            if (getActivity() != null) {
                                Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {


                                        if (TextUtils.isEmpty(pingresultTextView.getText())) {
                                            pingButton.setText("Pinging...");
                                            pingButton.setEnabled(false);
                                            try {
                                                doPing();

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                pingButton.setText("Ping");
                                                pingresultTextView.setText("Please give a Valid Ip Address");
                                                pingButton.setEnabled(true);
//                                            progressDialogStop();

                                            }
                                        } else {

                                            pingresultTextView.setText("");
                                            pingButton.setText("Pinging...");
                                            pingButton.setEnabled(false);


                                            try {
                                                doPing();

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                pingButton.setText("Ping");
                                                pingresultTextView.setText("Please give a Valid Ip Address");
                                                pingButton.setEnabled(true);
//                                           progressDialogStop();


                                            }

                                        }

                                    }

                                });
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            if (getActivity() != null) {
                                Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pingButton.setText("Ping");
                                        pingresultTextView.setText("Please give a Valid Ip Address");
                                        pingButton.setEnabled(true);


                                    }
                                });
                            }

                        }

                    }
                }).start();
            }
        });


        return view;

    }


    @Override
    public void onResume() {
        super.onResume();
        InetAddress ipAddress = IPTools.getLocalIPv4Address();
        if (ipAddress != null) {
            editIpAddress.setText(ipAddress.getHostAddress());
        } else errorTextview.setText("Please Connect to Network");


    }

    @Override
    public void onPause() {
        super.onPause();


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();


    }


    private void doPing() throws Exception {


        String ipAddress = editIpAddress.getText().toString();


        if (TextUtils.isEmpty(ipAddress)) {
            appendResultsText("Invalid Ip Address");

            pingButton.setEnabled(true);
            pingButton.setText("Ping");

            return;
        }

        // Perform a single synchronous ping
        PingResult pingResult = Ping.onAddress(ipAddress).setTimeOutMillis(1000).doPing();

        appendResultsText("Pinging Address: " + pingResult.getAddress().getHostAddress());
        appendResultsText("HostName: " + pingResult.getAddress().getHostName());
        appendResultsText(String.format("%.2f ms", pingResult.getTimeTaken()));


        // Perform an asynchronous ping
        Ping.onAddress(ipAddress).setTimeOutMillis(1000).setTimes(3).doPing(new Ping.PingListener() {
            @Override
            public void onResult(PingResult pingResult) {

                appendResultsText(String.format("%.2f ms", pingResult.getTimeTaken()));
            }

            @Override
            public void onFinished(PingStats pingStats) {


                appendResultsText(String.format("Pings: %d, Packets lost: %d",
                        pingStats.getNoPings(), pingStats.getPacketsLost()));
                appendResultsText(String.format("Min/Avg/Max Time: %.2f/%.2f/%.2f ms",
                        pingStats.getMinTimeTaken(), pingStats.getAverageTimeTaken(), pingStats.getMaxTimeTaken()));

                if (getActivity() != null) {
                    Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            pingButton.setText("Ping");
                            pingButton.setEnabled(true);


                        }
                    });
                }


            }

            @Override
            public void onError(Exception e) {
//                progressDialogStop();
                pingButton.setEnabled(true);

            }
        });


    }


    private void appendResultsText(final String text) {
        if (getActivity() != null) {
            Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    pingresultTextView.append(text + "\n");

                }
            });
        }
    }


}
