/*
 * Network Tools
 * Copyright (C) 2018  DataMedic Business IT Support <networktools@datamedic.gr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.datamedic.networktools.wifi.signal_meter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.datamedic.networktools.MainActivity;
import com.datamedic.networktools.R;

import java.util.ArrayList;
import java.util.List;

public class ScanList_SignalMeter {

    final CharSequence[] items = { "Take Photo From Gallery",
            "Take Photo From Camera" };
    Activity activity;
    AlertDialog dialog;
    AlertDialog.Builder builder;
    String detailProvader;



    public ScanList_SignalMeter(Activity a, String detailProvader) {
        this.activity = a;
        this.detailProvader = detailProvader;
        builder = new AlertDialog.Builder(a);
    }

    public void showDialog() {

        builder.setTitle("Wifi Access Point Details");
        builder.setMessage(detailProvader);


        AlertDialog alert = builder.create();
        alert.show();
    }
}