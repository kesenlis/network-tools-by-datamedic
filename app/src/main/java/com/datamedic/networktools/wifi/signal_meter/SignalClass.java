/*
 * Network Tools
 * Copyright (C) 2018  DataMedic Business IT Support <networktools@datamedic.gr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
// "This file was modified by John Poulikidis in 2019."

package com.datamedic.networktools.wifi.signal_meter;

public class SignalClass {
    private String SSID;
    private String BSSID;
    private String LingSpeed;
    private int frequence;
    private String band;



    private String Mhz;

    public SignalClass(String SSID, String BSSID, String lingSpeed, int frequence,String mhz,String band) {
        this.SSID = SSID;
        this.BSSID = BSSID;
        LingSpeed = lingSpeed;
        this.frequence = frequence;
        this.band = band;
        Mhz = mhz;
    }


    public String getSSID() {
        return SSID;
    }

    public void setSSID(String SSID) {
        this.SSID = SSID;
    }

    public String getBSSID() {
        return BSSID;
    }

    public void setBSSID(String BSSID) {
        this.BSSID = BSSID;
    }

    public String getLingSpeed() {
        return LingSpeed;
    }

    public void setLingSpeed(String lingSpeed) {
        LingSpeed = lingSpeed;
    }

    public int getFrequence() {
        return frequence;
    }

    public void setFrequence(int frequence) {
        this.frequence = frequence;
    }
    public String getBand() {
        return band;
    }

    public void setBand(String band) {
        this.band = band;
    }

    public String getMhz() {
        return Mhz;
    }

    public void setMhz(String mhz) {
        Mhz = mhz;
    }

}
