/*
 * Network Tools
 * Copyright (C) 2018  DataMedic Business IT Support <networktools@datamedic.gr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
// "This file was modified by John Poulikidis in 2019."

package com.datamedic.networktools.wifi.signal_meter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.datamedic.networktools.R;

import java.util.ArrayList;

public class SignalArray extends ArrayAdapter<SignalClass> {
    private int mresource;
    private Context mContext;
    TextView BandIcon;



    public SignalArray(@NonNull Context context, int resource, ArrayList object) {
        super(context, resource,object);
        mContext = context;
        mresource = resource;


    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        final String SSID = getItem(position).getSSID();
        final String BSSID = getItem(position).getBSSID();
        final String Link = getItem(position).getLingSpeed();
        final int Frequence = getItem(position).getFrequence();
        final String Band = getItem(position).getBand();
        final String Mhz = getItem(position).getMhz();


        final LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mresource, parent, false);


        TextView ssid = convertView.findViewById(R.id.ssid);
        TextView Bssid = convertView.findViewById(R.id.bssid);
      //  TextView LinkSpeed = convertView.findViewById(R.id.linkspeed);
        TextView Frenq = convertView.findViewById(R.id.freng);
        TextView mhz = convertView.findViewById(R.id.mhz);
        BandIcon = convertView.findViewById(R.id.bandicon);


        if (Frequence <=3000){
            BandIcon.setBackgroundResource(R.drawable.icon_2ghz);
        } else {
            BandIcon.setBackgroundResource(R.drawable.icon_5ghz);
        }



        ssid.setText(SSID);
        Bssid.setText(BSSID);
     //   LinkSpeed.setText(Link);
        Frenq.setText(String.valueOf(Frequence));
        mhz.setText(Mhz);


        return convertView;
    }
}
