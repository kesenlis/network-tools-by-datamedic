/*
 * Network Tools
 * Copyright (C) 2018  DataMedic Business IT Support <networktools@datamedic.gr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */



package com.datamedic.networktools.wifi.signal_meter;


import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.datamedic.networktools.R;


public class SignalMeterFragment extends Fragment implements Runnable {
    WifiInfo wifiInfo;

    static final int DELAY_INTERVAL = 900;

    int numberOfLevels = 100;
    int strengthnumber, macaddress, linkspeed, ipaddress;
    TextView str;
    WifiManager wifiManager;
    private Handler handler;
    private boolean running;
    TextView mainText, strength;
    public String ip2;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.signal_meter, container, false);



        handler = new Handler();



        wifiManager = (WifiManager) getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiInfo = wifiManager.getConnectionInfo();


        wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);


        ip2 = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());


        if (!wifiManager.isWifiEnabled()) {

            Toast.makeText(view.getContext(), "Wifi is disabled ... we need to enable it", Toast.LENGTH_LONG).show();
            wifiManager.setWifiEnabled(true);
        }


        run();


        return view;
    }


    @Override
    public void run() {

        Log.d("TASOSTEST",String.valueOf(wifiInfo.getBSSID()));

        if (wifiInfo.getBSSID() == null) {

            mainText.setText("Please connect to an active Wifi Network /  Access Point");
            strength.setText(String.valueOf(0));
            wifiManager = (WifiManager) getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            wifiInfo = wifiManager.getConnectionInfo();

        } else {

            showdata();


        }

        nextRun(DELAY_INTERVAL);


    }


    private void nextRun(int delayInitial) {
        stop();
        handler.postDelayed(this, delayInitial);
        running = true;


    }


    void stop() {
        handler.removeCallbacks(this);
        running = false;
    }


    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(this);
        running = false;
    }

    private void showdata() {


        strengthnumber = wifiInfo.getRssi();

        wifiInfo = wifiManager.getConnectionInfo();
        Log.d("SIGNAL12", String.valueOf(wifiInfo.getRssi()));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mainText.setText(String.valueOf(
                    " Current Connected Wifi Data:" +
                            "\n" +
                            "\n SSID : " + wifiInfo.getSSID() +
                            "\n Mac Address : " + wifiInfo.getBSSID() +
                            "\n Signal Strength : " + strengthnumber + "db" +
                            "\n Frequency : " + wifiInfo.getFrequency() + " Mhz" +
                            "\n Link Speed : " + wifiInfo.getLinkSpeed() + "Mbps" +
                            "\n Ip Address : " + ip2
            ));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            strength.setText(String.valueOf(wifiInfo.getRssi()));
        }


    }


}

