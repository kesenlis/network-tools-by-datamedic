/*
 * Network Tools
 * Copyright (C) 2018  DataMedic Business IT Support <networktools@datamedic.gr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
// "This file was modified by John Poulikidis in 2019."



package com.datamedic.networktools.wifi.signal_meter;


import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.datamedic.networktools.R;
import com.datamedic.networktools.ipcalculator.Converter;
import com.datamedic.networktools.wifi.accesspoint.AccessPointDetail;

import java.util.ArrayList;
import java.util.List;


public class SignalMeterFragment2 extends Fragment implements Runnable {

    static final int DELAY_INTERVAL = 1000;


    TextView str;
    WifiManager wifiManager;
    private Handler handler;
    private boolean running;
    String ip, SelectedSSID, CurrentSSID, LinkSpeed, MacAddress;

    final  ArrayList<SignalClass> theList = new ArrayList<>();
    int SelectedSignalLevel, Frequence;
    ScanResult PositionID;
    TextView SSIDtext, MacAddresstext, FrequencyText;
    public String ip2;
    Button listButton, soundBtn;
    private ListView listView;


    ScanResult scanResultSR;
    private List<ScanResult> results;
    SignalClass signalClass;
    private ArrayList<String> arrayList = new ArrayList<>();
    SignalArray signalArray;
    private ArrayAdapter adapter;
    public int positionItem;
    public boolean isClik;
    ImageView velona;
    MediaPlayer mp1;
    MediaPlayer mp2;
    MediaPlayer mp3;
    MediaPlayer mp4;
    public boolean soundon;
    int pivotX;
    int pivotY;
    private static final String TAG = SignalMeterFragment2.class.getSimpleName();


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.signal_meter2, container, false);


        SelectedSSID = "";

        mp4 = MediaPlayer.create(getContext(), R.raw.ultralow);
        mp3 = MediaPlayer.create(getContext(), R.raw.low);
        mp2 = MediaPlayer.create(getContext(), R.raw.medium);
        mp1 = MediaPlayer.create(getContext(), R.raw.full);

        soundon = true;

        listButton = view.findViewById(R.id.listbutton);


        handler = new Handler();
        SSIDtext = view.findViewById(R.id.SSIDText);
        str = view.findViewById(R.id.strenght);
        MacAddresstext = view.findViewById(R.id.MacAddressText);
        FrequencyText = view.findViewById(R.id.FrequencyText);
        velona = view.findViewById(R.id.velona);
        soundBtn = view.findViewById(R.id.soundBtn);

        pivotX = velona.getMeasuredWidth() / 2;
        pivotY = velona.getMeasuredHeight() - velona.getMeasuredWidth() / 2;
        velona.setPivotX(pivotX);
        velona.setPivotY(pivotY);


        wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (!wifiManager.isWifiEnabled()) {
            Toast.makeText(view.getContext(), R.string.wifiisdisabled, Toast.LENGTH_LONG).show();
            wifiManager.setWifiEnabled(true);
        }


        run();


        soundBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (soundon) {

                    soundon = false;
                    soundBtn.setText(R.string.enablesound);

                } else {

                    soundon = true;
                    soundBtn.setText(R.string.disablesound);

                }

            }
        });


        listButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                signalClass = new SignalClass(CurrentSSID, MacAddress,LinkSpeed,Frequence,MacAddress,MacAddress);
                theList.add(signalClass);
                signalArray = new SignalArray(view.getContext(), R.layout.signal_meter,theList);


                signalArray.clear();


                scanWifi();
                LayoutInflater inflater = ((LayoutInflater) view.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE));
                View customView1 = inflater.inflate(R.layout.scanlist_signalmeter, null, false);
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(view.getContext());
                mBuilder.setTitle("Available Access Points");


                listView = customView1.findViewById(R.id.List);
                listView.setAdapter(signalArray);


                mBuilder.setView(customView1);
                final AlertDialog dialog = mBuilder.create();



                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        positionItem = position;
                        SelectedSSID = arrayList.get(position);
                        CurrentSSID = results.get(position).SSID;
                        MacAddress = results.get(position).BSSID;
                        LinkSpeed = results.get(position).capabilities;
                        Frequence = results.get(position).frequency;


                        scanResultSR = results.get(position);
                        SelectedSignalLevel = results.get(position).level;
                        PositionID = results.get(position);
                        isClik = true;

                        dialog.cancel();

                    }


                });


                dialog.show();
            }
        });


        return view;
    }

    @Override
    public void onStart() {
        Log.d("ONSTART", "CALLED ON START");
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("ONRESUME", "CALLED ON RESUME");
        SelectedSignalLevel = 0;
        run();


    }


    public void run() {

        pivotX = velona.getMeasuredWidth() / 2;
        pivotY = velona.getMeasuredHeight() - velona.getMeasuredWidth() / 2;

        velona.setPivotX(pivotX);
        velona.setPivotY(pivotY);

        Log.d("SCAN1212", "RUN");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            if (isAdded()) {
                SSIDtext.setText(String.valueOf(getString(R.string.SMSSID) + CurrentSSID
                ));
                MacAddresstext.setText(String.valueOf(
                        "Mac Address : \n" + MacAddress
                ));
                FrequencyText.setText(String.valueOf(
                        getString(R.string.FREqSignalmeter) + Frequence + " Mhz"
                ));

                str.setText(String.valueOf(

                        SelectedSignalLevel
                ));
                Log.d("Signal", String.valueOf(SelectedSignalLevel));
                if (AccessPointDetail.start) {
                    AccessPointDetail.text(str);
                }


                velona.animate().rotation(SelectedSignalLevel * 2).setDuration(100);

                if (soundon) {


                    if (SelectedSignalLevel >= -39 & SelectedSignalLevel <= -1) {

                        mp1.start();
                    }
                    if (SelectedSignalLevel <= -40 & SelectedSignalLevel >= -60) {

                        mp2.start();

                    }
                    if (SelectedSignalLevel <= -60 & SelectedSignalLevel >= -75) {

                        mp3.start();

                    }
                    if (SelectedSignalLevel <= -75 & SelectedSignalLevel >= -95) {

                        mp4.start();

                    }

                    if (SelectedSignalLevel == 0) {

                        if (mp1.isPlaying()) {
                            mp1.pause();
                        }
                        if (mp2.isPlaying()) {
                            mp2.pause();
                        }
                        if (mp3.isPlaying()) {

                            mp3.pause();
                        }
                        if (mp4.isPlaying()) {

                            mp4.pause();
                        }
                    }


                }
            }
        }

        nextRun(DELAY_INTERVAL);


    }


    private void nextRun(int delayInitial) {
        stop();
        handler.postDelayed(this, delayInitial);
        running = true;


    }


    void stop() {
        handler.removeCallbacks(this);
        running = false;
    }


    private void scanWifi() {
        Log.d("SCANWIFI", "Scanwifi");
        signalArray.clear();
        arrayList.clear();
        wifiManager.startScan();
        final IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.RSSI_CHANGED_ACTION);
        filter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        filter.addAction(WifiManager.ACTION_REQUEST_SCAN_ALWAYS_AVAILABLE);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.EXTRA_RESULTS_UPDATED);
        filter.addAction(WifiManager.EXTRA_NETWORK_INFO);


        getActivity().registerReceiver(wifiReceiver, filter);
        Toast.makeText(getContext(), R.string.scanningaccesspoints, Toast.LENGTH_SHORT).show();
    }


    BroadcastReceiver wifiReceiver = new BroadcastReceiver() {


        @Override
        public void onReceive(Context context, Intent intent) {
            signalArray.clear();
            arrayList.clear();
            //     Log.d("SCANWIFI", "Onreceive");
            results = wifiManager.getScanResults();


            for (ScanResult scanResult : results) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    String Mhz = "";


                    //      Log.d("TIPOSE", String.valueOf(scanResult.channelWidth));
                    if (scanResult.channelWidth == 0) {

                        Mhz = "20";
                    } else if (scanResult.channelWidth == 1) {

                        Mhz = "40";

                    } else if (scanResult.channelWidth == 2) {

                        Mhz = "80";

                    }

                    double Band = 0;

                    if (scanResult.frequency <= 2500) {

                        Band = 2.4;
                    } else {

                        Band = 5.0;
                    }

                    signalClass = new SignalClass( scanResult.SSID,scanResult.BSSID,scanResult.capabilities,scanResult.frequency
                            ,Mhz,scanResult.BSSID);

                    theList.add(signalClass);

                    arrayList.add("SSID  : " + scanResult.SSID +
                            "\n" +
                            "Band : " + Band + " Ghz" + "    Ch.Width : " + Mhz + " Mhz" +
                            "\n" +  "Mac : " + scanResult.BSSID);
                }




                signalArray.notifyDataSetChanged();

            }
            positionItem = arrayList.indexOf(SelectedSSID);
            //     Log.d("SCAN", "SellectedSSID : " + SelectedSSID);

            if (isClik) {

                if (positionItem == -1) {
                    Toast.makeText(getContext(), R.string.signalost, Toast.LENGTH_SHORT).show();
                    SelectedSignalLevel = 0;
//                    Log.d("SCAN", "AKRIO : " + SelectedSSID);


                } else {
                    SelectedSignalLevel = results.get(positionItem).level;
                    CurrentSSID = results.get(positionItem).SSID;
                    Frequence = results.get(positionItem).frequency;
                    MacAddress = results.get(positionItem).BSSID;
                    //      Log.d("SCAN", "TO PIRE : " + SelectedSSID);


                }
            }
        }


    };


    @Override
    public void onPause() {
        super.onPause();

        Log.d("ONPAUSE", "ONPAUSE");
        Frequence = 0;
        CurrentSSID = "null";
        MacAddress = "null";
        SelectedSSID = "";
        if (signalArray != null){
            signalArray.clear();
        }
        arrayList.clear();
        if (isClik) {
            getActivity().unregisterReceiver(wifiReceiver);
            isClik = false;

            Log.d("ONDESTROYVIEW", "MP RELEASE");
            handler.removeCallbacks(this);
            running = false;


        }
        stop();

    }

    @Override
    public void onDestroyView() {
        Log.d("ONDESTROYVIEW", "ONDESTROYVIEW");
        super.onDestroyView();
        if (isClik) {
            getActivity().unregisterReceiver(wifiReceiver);
            isClik = false;
            mp1.release();
            mp2.release();
            mp3.release();
            mp4.release();
            Log.d("ONDESTROYVIEW", "MP RELEASE");
            handler.removeCallbacks(this);
            running = false;


        }

    }




}


