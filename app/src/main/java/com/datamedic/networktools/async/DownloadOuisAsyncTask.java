package com.datamedic.networktools.async;


import com.datamedic.networktools.activity.LanScanner;
import com.datamedic.networktools.db.Database;
import com.datamedic.networktools.parser.Parser;
import com.datamedic.networktools.response.MainAsyncResponse;

import java.lang.ref.WeakReference;

public class DownloadOuisAsyncTask extends DownloadAsyncTask {

    private static final String SERVICE = "https://code.wireshark.org/review/gitweb?p=wireshark.git;a=blob_plain;f=manuf";

    /**
     * Creates a new asynchronous task to handle downloading OUI data.
     *
     * @param database
     * @param parser
     * @param activity
     */
    public DownloadOuisAsyncTask(Database database, Parser parser, MainAsyncResponse activity) {
        db = database;
        delegate = new WeakReference<>(activity);
        this.parser = parser;
    }

    /**
     * Sets up and displays the dialog.
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * Downloads new OUI data.
     *
     * @param params
     * @return
     */
    @Override
    protected Void doInBackground(Void... params) {
        db.clearOuis();
        doInBackground(SERVICE, parser);
        return null;
    }

    /**
     * Updates the UI with the MAC address from the newly fetched data.
     *
     * @param result
     */
    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        MainAsyncResponse activity = delegate.get();
        if (activity != null) {
            ((LanScanner) activity).setupMac();
        }
    }

}