/*
This file is part of the project TraceroutePing, which is an Android library
implementing Traceroute with ping under GPL license v3.
Copyright (C) 2013  Olivier Goutay

TraceroutePing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TraceroutePing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TraceroutePing.  If not, see <http://www.gnu.org/licenses/>.
 */

// "This file was modified by Tasos Kesenlis in 2019."

package com.datamedic.networktools.traceroute;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.datamedic.networktools.R;

import java.util.ArrayList;
import java.util.List;


public class TraceRouteFragment extends Fragment {


    public static final String tag = "TraceroutePing";
    public static final String INTENT_TRACE = "INTENT_TRACE";


    private Button buttonLaunch;
    private EditText editTextPing;
    private ProgressBar progressBarPing;
    private ListView listViewTraceroute;
    private TraceListAdapter traceListAdapter;
    protected ProgressDialog scanProgressDialog;

    private TracerouteWithPing tracerouteWithPing;
    private final int maxTtl = 40;

    private List<TracerouteContainer> traces;

    /**
     * onCreate, init main components from view
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.traceroute, container, false);

        this.tracerouteWithPing = new TracerouteWithPing(this);
        this.traces = new ArrayList<TracerouteContainer>();


        this.buttonLaunch = (Button) view.findViewById(R.id.buttonLaunch);
        this.editTextPing = (EditText) view.findViewById(R.id.editTextPing);
        this.listViewTraceroute = (ListView) view.findViewById(R.id.listViewTraceroute);
        this.progressBarPing = (ProgressBar) view.findViewById(R.id.progressBarPing);

        initView();



        return view;
    }

    /**
     * initView, init the main view components (action, adapter...)
     */
    private void initView() {
        buttonLaunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextPing.getText().length() == 0) {
                    Toast.makeText(getContext(), getString(R.string.no_text_trace), Toast.LENGTH_SHORT).show();
                } else {
                    traces.clear();
                    traceListAdapter.notifyDataSetChanged();

                    startProgressBar();
                    hideSoftwareKeyboard(editTextPing);
                    tracerouteWithPing.executeTraceroute(editTextPing.getText().toString(), maxTtl);
                }
            }
        });

        traceListAdapter = new TraceListAdapter(getContext());
        listViewTraceroute.setAdapter(traceListAdapter);
    }

    /**
     * Allows to refresh the listview of traces
     */
    public void refreshList(TracerouteContainer trace) {
        final TracerouteContainer fTrace = trace;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                traces.add(fTrace);
                traceListAdapter.notifyDataSetChanged();
            }
        });
    }


    /**
     * The adapter of the listview (build the views)
     */
    public class TraceListAdapter extends BaseAdapter {

        private Context context;

        public TraceListAdapter(Context c) {
            context = c;
        }

        public int getCount() {
            return traces.size();
        }

        public TracerouteContainer getItem(int position) {
            return traces.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            // first init
            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.item_list_trace, null);

                TextView textViewNumber = (TextView) convertView.findViewById(R.id.textViewNumber);
                TextView textViewIp = (TextView) convertView.findViewById(R.id.textViewIp);
                TextView textViewTime = (TextView) convertView.findViewById(R.id.textViewTime);
                ImageView imageViewStatusPing = (ImageView) convertView.findViewById(R.id.imageViewStatusPing);

                // Set up the ViewHolder.
                holder = new ViewHolder();
                holder.textViewNumber = textViewNumber;
                holder.textViewIp = textViewIp;
                holder.textViewTime = textViewTime;
                holder.imageViewStatusPing = imageViewStatusPing;

                // Store the holder with the view.
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            TracerouteContainer currentTrace = getItem(position);

            if (position % 2 == 1) {
                convertView.setBackgroundResource(R.drawable.table_odd_lines);
            } else {
                convertView.setBackgroundResource(R.drawable.table_pair_lines);
            }

            if (currentTrace.isSuccessful()) {
                holder.imageViewStatusPing.setImageResource(R.drawable.check);
            } else {
                holder.imageViewStatusPing.setImageResource(R.drawable.cross);
            }

            holder.textViewNumber.setText(position + "");
            holder.textViewIp.setText(currentTrace.getHostname() + " (" + currentTrace.getIp() + ")");
            holder.textViewTime.setText(currentTrace.getMs() + "ms");

            return convertView;
        }

        // ViewHolder pattern
        class ViewHolder {
            TextView textViewNumber;
            TextView textViewIp;
            TextView textViewTime;
            ImageView imageViewStatusPing;
        }
    }

    /**
     * Hides the keyboard
     *
     * @param currentEditText The current selected edittext
     */
    public void hideSoftwareKeyboard(EditText currentEditText) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive()) {
            imm.hideSoftInputFromWindow(currentEditText.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void startProgressBar() {
//        progressBarPing.setVisibility(View.VISIBLE);
//        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        scanProgressDialog = new ProgressDialog(getContext(), R.style.NewDialog);
        scanProgressDialog.setCancelable(false);
        scanProgressDialog.getWindow().setGravity(Gravity.CENTER);

        scanProgressDialog.setTitle("Tracing.... Please wait");
        scanProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        scanProgressDialog.show();


    }

    public void stopProgressBar() {
//        progressBarPing.setVisibility(View.GONE);
//        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        scanProgressDialog.dismiss();


    }


    @Override
    public void onPause() {
        super.onPause();

    }


}





