package com.datamedic.networktools.speedtest;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.datamedic.networktools.MainContext;
import com.datamedic.networktools.network.Host;
import com.datamedic.networktools.R;
import com.datamedic.networktools.response.MainAsyncResponse;
import com.datamedic.util.UserPreference;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import androidx.fragment.app.Fragment;


// "This file was modified by Tasos Kesenlis in 2019."


public class SpeedTestFragment extends Fragment implements MainAsyncResponse {


    static int position = 0;
    static int lastPosition = 0;
    GetSpeedTestHostsHandler getSpeedTestHostsHandler = null;
    HashSet<String> tempBlackList;
    protected ProgressDialog scanProgressDialog;
    ConnectivityManager connManager;
    NetworkInfo mWifi;
    Button startButton;
    private com.datamedic.networktools.network.Wireless wifi;
    private String cachedWanIp = "";
    ConnectivityManager connectivityManager;
    NetworkInfo[] networkInfos;
    boolean haveIp = false;
    final DecimalFormat dec = new DecimalFormat("#.##");
    View view;
    @Override
    public void onResume() {
        super.onResume();
        MainContext.INSTANCE.getScannerService().pause();

        getSpeedTestHostsHandler = new GetSpeedTestHostsHandler();
        getSpeedTestHostsHandler.start();
    }


    public SpeedTestFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       view = inflater.inflate(R.layout.fragment_speed_test, container, false);

        startButton = view.findViewById(R.id.startButton);

        startButton.setText(R.string.begin_test);

        tempBlackList = new HashSet<>();

        wifi = new com.datamedic.networktools.network.Wireless(getContext());


        getSpeedTestHostsHandler = new GetSpeedTestHostsHandler();
        getSpeedTestHostsHandler.start();

        connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfos = connectivityManager.getAllNetworkInfo();


        Log.d("tophreIp", cachedWanIp + " ok");

        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startButton.setEnabled(false);
                pausescreen();
                getExternalIp();


            }
        });


        return view;
    }


    public int getPositionByRate(double rate) {
        if (rate <= 1) {
            return (int) (rate * 30);

        } else if (rate <= 10) {
            return (int) (rate * 6) + 30;

        } else if (rate <= 30) {
            return (int) ((rate - 10) * 3) + 90;

        } else if (rate <= 50) {
            return (int) ((rate - 30) * 1.5) + 150;

        } else if (rate <= 100) {
            return (int) ((rate - 50) * 1.2) + 180;
        }

        return 0;
    }

    @Override
    public void onPause() {
        super.onPause();
        MainContext.INSTANCE.getScannerService().resume();
        MainContext.INSTANCE.getMainActivity().update();
    }


    public void pausescreen() {


        scanProgressDialog = new ProgressDialog(getContext(), R.style.SpeedTestDialog);
        scanProgressDialog.setCancelable(false);
        scanProgressDialog.getWindow().setGravity(Gravity.CENTER);
        scanProgressDialog.setTitle(getString(R.string.checking_internet));
        scanProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        scanProgressDialog.show();


    }

    public void unpausescreen() {

        scanProgressDialog.dismiss();
        haveIp = false;
        startButton.setEnabled(true);



    }


    private void getExternalIp() {

        if (UserPreference.getFetchExternalIp(getContext())) {


            wifi.getExternalIpAddress(this);


        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedState) {
        onRestoreInstanceState(savedState);

        cachedWanIp = savedState.getString("wanIp");
    }

    @Override
    public void processFinish(Host h, AtomicInteger i) {
        Log.d("teleiwse", "ok1");
    }

    @Override
    public void processFinish(int output) {

        Log.d("teleiwse", "ok2" + output);
    }

    @Override
    public void processFinish(String output) {
        cachedWanIp = output;
        if (!cachedWanIp.equals("Couldn't get your external IP")) {
            haveIp = true;
            Log.d("teleiwse", "etoimazete" + haveIp);
            Log.d("teleiwse", "ok2,5" + output);

            test(view);


        } else {
            haveIp = false;
            startButton.setEnabled(true);
            unpausescreen();
            Toast.makeText(getContext(),"No internet Connection",Toast.LENGTH_SHORT).show();
        }
        Log.d("teleiwse", "ok3" + output);
    }

    @Override
    public void processFinish(boolean output) {


    }

    @Override
    public <T extends Throwable> void processFinish(T output) {

    }
    public void test(View view){

        if (getSpeedTestHostsHandler == null) {
            getSpeedTestHostsHandler = new GetSpeedTestHostsHandler();
            getSpeedTestHostsHandler.start();
        }

        new Thread(new Runnable() {
            RotateAnimation rotate;
            ImageView barImageView = (ImageView) view.findViewById(R.id.barImageView);
            TextView pingTextView = (TextView) view.findViewById(R.id.pingTextView);
            TextView downloadTextView = (TextView) view.findViewById(R.id.downloadTextView);
            TextView uploadTextView = (TextView) view.findViewById(R.id.uploadTextView);

            @Override
            public void run() {

                if (haveIp) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            scanProgressDialog.setTitle(getString(R.string.performing));
                        }
                    });


                    Log.d("teleiwse", "egine ");
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            startButton.setText(R.string.selectingbestserver);
                        }
                    });


                    int timeCount = 600; //1min
                    while (!getSpeedTestHostsHandler.isFinished()) {
                        timeCount--;
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {

                        }
                        if (timeCount <= 0) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getContext(), "No Connection...", Toast.LENGTH_LONG).show();
                                    startButton.setEnabled(true);
                                    startButton.setTextSize(16);
                                    startButton.setText(R.string.restarttest);
                                    unpausescreen();


                                }
                            });

                            getSpeedTestHostsHandler = null;
                            return;
                        }
                    }

                    //Find closest server
                    HashMap<Integer, String> mapKey = getSpeedTestHostsHandler.getMapKey();
                    HashMap<Integer, List<String>> mapValue = getSpeedTestHostsHandler.getMapValue();
                    double selfLat = getSpeedTestHostsHandler.getSelfLat();
                    double selfLon = getSpeedTestHostsHandler.getSelfLon();
                    double tmp = 19349458;
                    double dist = 0.0;
                    int findServerIndex = 0;
                    for (int index : mapKey.keySet()) {
                        if (tempBlackList.contains(mapValue.get(index).get(5))) {
                            continue;
                        }

                        Location source = new Location("Source");
                        source.setLatitude(selfLat);
                        source.setLongitude(selfLon);

                        List<String> ls = mapValue.get(index);
                        Location dest = new Location("Dest");
                        dest.setLatitude(Double.parseDouble(ls.get(0)));
                        dest.setLongitude(Double.parseDouble(ls.get(1)));

                        double distance = source.distanceTo(dest);
                        if (tmp > distance) {
                            tmp = distance;
                            dist = distance;
                            findServerIndex = index;
                        }
                    }

                    String uploadAddr = mapKey.get(findServerIndex);
                    final List<String> info = mapValue.get(findServerIndex);
                    final double distance = dist;

                    if (info == null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startButton.setTextSize(12);
                                startButton.setText(R.string.therewasaproblemgettinglocation);
                                unpausescreen();
                            }
                        });
                        return;
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            startButton.setTextSize(13);
                            startButton.setText(String.format("Host Location: %s [Distance: %s km]", info.get(2), new DecimalFormat("#.##").format(distance / 1000)));
                        }
                    });

                    //Init Ping graphic
                    final LinearLayout chartPing = (LinearLayout) view.findViewById(R.id.chartPing);
                    XYSeriesRenderer pingRenderer = new XYSeriesRenderer();
                    XYSeriesRenderer.FillOutsideLine pingFill = new XYSeriesRenderer.FillOutsideLine(XYSeriesRenderer.FillOutsideLine.Type.BOUNDS_ALL);
                    pingFill.setColor(Color.parseColor("#4d5a6a"));
                    pingRenderer.addFillOutsideLine(pingFill);
                    pingRenderer.setDisplayChartValues(false);
                    pingRenderer.setShowLegendItem(false);
                    pingRenderer.setColor(Color.parseColor("#4d5a6a"));
                    pingRenderer.setLineWidth(5);
                    final XYMultipleSeriesRenderer multiPingRenderer = new XYMultipleSeriesRenderer();
                    multiPingRenderer.setXLabels(0);
                    multiPingRenderer.setYLabels(0);
                    multiPingRenderer.setZoomEnabled(false);
                    multiPingRenderer.setXAxisColor(Color.parseColor("#647488"));
                    multiPingRenderer.setYAxisColor(Color.parseColor("#2F3C4C"));
                    multiPingRenderer.setPanEnabled(true, true);
                    multiPingRenderer.setZoomButtonsVisible(false);
                    multiPingRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00));
                    multiPingRenderer.addSeriesRenderer(pingRenderer);

                    //Init Download graphic
                    final LinearLayout chartDownload = (LinearLayout) view.findViewById(R.id.chartDownload);
                    XYSeriesRenderer downloadRenderer = new XYSeriesRenderer();
                    XYSeriesRenderer.FillOutsideLine downloadFill = new XYSeriesRenderer.FillOutsideLine(XYSeriesRenderer.FillOutsideLine.Type.BOUNDS_ALL);
                    downloadFill.setColor(Color.parseColor("#4d5a6a"));
                    downloadRenderer.addFillOutsideLine(downloadFill);
                    downloadRenderer.setDisplayChartValues(false);
                    downloadRenderer.setColor(Color.parseColor("#4d5a6a"));
                    downloadRenderer.setShowLegendItem(false);
                    downloadRenderer.setLineWidth(5);
                    final XYMultipleSeriesRenderer multiDownloadRenderer = new XYMultipleSeriesRenderer();
                    multiDownloadRenderer.setXLabels(0);
                    multiDownloadRenderer.setYLabels(0);
                    multiDownloadRenderer.setZoomEnabled(false);
                    multiDownloadRenderer.setXAxisColor(Color.parseColor("#647488"));
                    multiDownloadRenderer.setYAxisColor(Color.parseColor("#2F3C4C"));
                    multiDownloadRenderer.setPanEnabled(false, false);
                    multiDownloadRenderer.setZoomButtonsVisible(false);
                    multiDownloadRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00));
                    multiDownloadRenderer.addSeriesRenderer(downloadRenderer);

                    //Init Upload graphic
                    final LinearLayout chartUpload = (LinearLayout) view.findViewById(R.id.chartUpload);
                    XYSeriesRenderer uploadRenderer = new XYSeriesRenderer();
                    XYSeriesRenderer.FillOutsideLine uploadFill = new XYSeriesRenderer.FillOutsideLine(XYSeriesRenderer.FillOutsideLine.Type.BOUNDS_ALL);
                    uploadFill.setColor(Color.parseColor("#4d5a6a"));
                    uploadRenderer.addFillOutsideLine(uploadFill);
                    uploadRenderer.setDisplayChartValues(false);
                    uploadRenderer.setColor(Color.parseColor("#4d5a6a"));
                    uploadRenderer.setShowLegendItem(false);
                    uploadRenderer.setLineWidth(5);
                    final XYMultipleSeriesRenderer multiUploadRenderer = new XYMultipleSeriesRenderer();
                    multiUploadRenderer.setXLabels(0);
                    multiUploadRenderer.setYLabels(0);
                    multiUploadRenderer.setZoomEnabled(false);
                    multiUploadRenderer.setXAxisColor(Color.parseColor("#647488"));
                    multiUploadRenderer.setYAxisColor(Color.parseColor("#2F3C4C"));
                    multiUploadRenderer.setPanEnabled(false, false);
                    multiUploadRenderer.setZoomButtonsVisible(false);
                    multiUploadRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00));
                    multiUploadRenderer.addSeriesRenderer(uploadRenderer);

                    //Reset value, graphics
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pingTextView.setText("0 ms");
                            chartPing.removeAllViews();
                            downloadTextView.setText("0 Mbps");
                            chartDownload.removeAllViews();
                            uploadTextView.setText("0 Mbps");
                            chartUpload.removeAllViews();
                        }
                    });
                    final List<Double> pingRateList = new ArrayList<>();
                    final List<Double> downloadRateList = new ArrayList<>();
                    final List<Double> uploadRateList = new ArrayList<>();
                    Boolean pingTestStarted = false;
                    Boolean pingTestFinished = false;
                    Boolean downloadTestStarted = false;
                    Boolean downloadTestFinished = false;
                    Boolean uploadTestStarted = false;
                    Boolean uploadTestFinished = false;

                    //Init Test
                    final PingTest pingTest = new PingTest(info.get(6).replace(":8080", ""), 6);
                    final HttpDownloadTest downloadTest = new HttpDownloadTest(uploadAddr.replace(uploadAddr.split("/")[uploadAddr.split("/").length - 1], ""));
                    final HttpUploadTest uploadTest = new HttpUploadTest(uploadAddr);


                    //Tests
                    while (true) {
                        if (!pingTestStarted) {
                            pingTest.start();
                            pingTestStarted = true;
                        }
                        if (pingTestFinished && !downloadTestStarted) {
                            downloadTest.start();
                            downloadTestStarted = true;
                        }
                        if (downloadTestFinished && !uploadTestStarted) {
                            uploadTest.start();
                            uploadTestStarted = true;
                        }


                        //Ping Test
                        if (pingTestFinished) {
                            //Failure
                            if (pingTest.getAvgRtt() == 0) {
                                System.out.println("Ping error...");
                            } else {
                                //Success
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pingTextView.setText(dec.format(pingTest.getAvgRtt()) + " ms");
                                    }
                                });
                            }
                        } else {
                            pingRateList.add(pingTest.getInstantRtt());

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    pingTextView.setText(dec.format(pingTest.getInstantRtt()) + " ms");
                                }
                            });

                            //Update chart
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    // Creating an  XYSeries for Income
                                    XYSeries pingSeries = new XYSeries("");
                                    pingSeries.setTitle("");

                                    int count = 0;
                                    List<Double> tmpLs = new ArrayList<>(pingRateList);
                                    for (Double val : tmpLs) {
                                        pingSeries.add(count++, val);
                                    }

                                    XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
                                    dataset.addSeries(pingSeries);


                                    GraphicalView chartView = ChartFactory.getLineChartView(getContext(), dataset, multiPingRenderer);
                                    chartPing.addView(chartView, 0);


                                }
                            });
                        }



                        //Download Test
                        if (pingTestFinished) {
                            if (downloadTestFinished) {
                                //Failure
                                if (downloadTest.getFinalDownloadRate() == 0) {
                                    System.out.println("Download error...");
                                } else {
                                    //Success
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            downloadTextView.setText(dec.format(downloadTest.getFinalDownloadRate()) + " Mbps");
                                        }
                                    });
                                }
                            } else {
                                //Calc position
                                double downloadRate = downloadTest.getInstantDownloadRate();
                                downloadRateList.add(downloadRate);
                                position = getPositionByRate(downloadRate);

                                getActivity().runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        rotate = new RotateAnimation(lastPosition, position, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                        rotate.setInterpolator(new LinearInterpolator());
                                        rotate.setDuration(100);
                                        barImageView.startAnimation(rotate);
                                        downloadTextView.setText(dec.format(downloadTest.getInstantDownloadRate()) + " Mbps");

                                    }

                                });
                                lastPosition = position;


                                //Update chart
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Creating an  XYSeries for Income
                                        XYSeries downloadSeries = new XYSeries("");
                                        downloadSeries.setTitle("");

                                        List<Double> tmpLs = new ArrayList<>(downloadRateList);
                                        int count = 0;
                                        for (Double val : tmpLs) {
                                            downloadSeries.add(count++, val);
                                        }

                                        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
                                        dataset.addSeries(downloadSeries);

                                        GraphicalView chartView = ChartFactory.getLineChartView(getContext(), dataset, multiDownloadRenderer);
                                        chartDownload.addView(chartView, 0);

                                    }
                                });


                            }
                        }


                        //Upload Test
                        if (downloadTestFinished) {
                            if (uploadTestFinished) {
                                //Failure
                                if (uploadTest.getFinalUploadRate() == 0) {
                                    System.out.println("Upload error...");
                                } else {
                                    //Success
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            uploadTextView.setText(dec.format(uploadTest.getFinalUploadRate()) + " Mbps");
                                        }
                                    });
                                }
                            } else {
                                //Calc position
                                double uploadRate = uploadTest.getInstantUploadRate();
                                uploadRateList.add(uploadRate);
                                position = getPositionByRate(uploadRate);

                                getActivity().runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        rotate = new RotateAnimation(lastPosition, position, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                        rotate.setInterpolator(new LinearInterpolator());
                                        rotate.setDuration(100);
                                        barImageView.startAnimation(rotate);
                                        uploadTextView.setText(dec.format(uploadTest.getInstantUploadRate()) + " Mbps");
                                    }

                                });
                                lastPosition = position;

                                //Update chart
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Creating an  XYSeries for Income
                                        XYSeries uploadSeries = new XYSeries("");
                                        uploadSeries.setTitle("");

                                        int count = 0;
                                        List<Double> tmpLs = new ArrayList<>(uploadRateList);
                                        for (Double val : tmpLs) {
                                            if (count == 0) {
                                                val = 0.0;
                                            }
                                            uploadSeries.add(count++, val);
                                        }

                                        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
                                        dataset.addSeries(uploadSeries);

                                        GraphicalView chartView = ChartFactory.getLineChartView(getContext(), dataset, multiUploadRenderer);
                                        chartUpload.addView(chartView, 0);
                                    }
                                });

                            }
                        }


                        //Test bitti
                        if (pingTestFinished && downloadTestFinished && uploadTest.isFinished()) {
                            break;
                        }

                        if (pingTest.isFinished()) {
                            pingTestFinished = true;
                        }
                        if (downloadTest.isFinished()) {
                            downloadTestFinished = true;
                        }
                        if (uploadTest.isFinished()) {
                            uploadTestFinished = true;
                        }

                        if (pingTestStarted && !pingTestFinished) {
                            try {
                                Thread.sleep(300);
                            } catch (InterruptedException e) {
                            }
                        } else {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                            }
                        }
                    }


                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            startButton.setEnabled(true);
                            startButton.setTextSize(16);
                            startButton.setText(R.string.restarttest);
                            unpausescreen();
                        }
                    });


                }else{
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            unpausescreen();
                        }
                    });
                }

            }
        }).start();
        Log.d("tophreIp", cachedWanIp + " ok");


    }

}


