/*
 * WiFiAnalyzer
 * Copyright (C) 2018  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.datamedic.networktools.navigation.items;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.MenuItem;

import android.widget.Toast;

import androidx.annotation.NonNull;

import com.datamedic.networktools.MainActivity;

import com.datamedic.networktools.navigation.NavigationMenu;


class IntentItem implements NavigationItem {

    MainActivity mainActivity;


    @Override
    public void activate(@NonNull MainActivity mainActivity, @NonNull MenuItem menuItem, @NonNull NavigationMenu navigationMenu) {


        try {
            Intent viewIntent =
                    new Intent("android.intent.action.VIEW",
                            Uri.parse("market://details?id=com.datamedic.deviceinfo"));
            mainActivity.startActivity(viewIntent);
        }catch(Exception e) {
            Toast.makeText(mainActivity.getApplicationContext(),"Unable to Connect Try Again...",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }


    }



    @Override
    public boolean isRegistered() {
        return false;
    }

    @Override
    public int getVisibility() {
        return 0;
    }
}

