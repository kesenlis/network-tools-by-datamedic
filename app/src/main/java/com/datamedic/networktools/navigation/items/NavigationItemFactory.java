/*
 * WiFiAnalyzer
 * Copyright (C) 2018  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

// "This file was modified by John Poulikidis in 2019."

package com.datamedic.networktools.navigation.items;

import android.view.View;

import com.datamedic.networktools.about.AboutFragment;
import com.datamedic.networktools.activity.LanScanner;
import com.datamedic.networktools.activity.WanHostActivity;
import com.datamedic.networktools.domainwhois.DomainFragment;
import com.datamedic.networktools.ipcalculator.Converter;
import com.datamedic.networktools.ipcalculator.IpV4Calculator;
import com.datamedic.networktools.openportool.OpenPortTestFragment;
import com.datamedic.networktools.settings.SettingsFragment;
import com.datamedic.networktools.speedtest.SpeedTestFragment;
import com.datamedic.networktools.traceroute.TraceRouteFragment;
import com.datamedic.networktools.vendor.VendorFragment;
import com.datamedic.networktools.wifi.accesspoint.AccessPointsFragment;
import com.datamedic.networktools.wifi.channelavailable.ChannelAvailableFragment;
import com.datamedic.networktools.wifi.channelgraph.ChannelGraphFragment;
import com.datamedic.networktools.wifi.channelrating.ChannelRatingFragment;
import com.datamedic.networktools.wifi.ping.PingFragment;
import com.datamedic.networktools.wifi.signal_meter.SignalMeterFragment2;
import com.datamedic.networktools.wifi.timegraph.TimeGraphFragment;

import java.net.PortUnreachableException;

public class NavigationItemFactory {
    public static final NavigationItem ACCESS_POINTS = new FragmentItem(new AccessPointsFragment());
    public static final NavigationItem CHANNEL_RATING = new FragmentItem(new ChannelRatingFragment());
    public static final NavigationItem CHANNEL_GRAPH = new FragmentItem(new ChannelGraphFragment());
    public static final NavigationItem TIME_GRAPH = new FragmentItem(new TimeGraphFragment());
    public static final NavigationItem EXPORT = new ExportItem();
    public static final NavigationItem CHANNEL_AVAILABLE = new FragmentItem(new ChannelAvailableFragment());
    public static final NavigationItem VENDORS = new FragmentItem(new VendorFragment(),false, View.GONE);
    public static final NavigationItem SETTINGS = new FragmentItem(new SettingsFragment(),false, View.GONE);
    public static final NavigationItem ABOUT = new FragmentItem(new AboutFragment(),false, View.GONE);
    public static final NavigationItem LANSCANNER = new FragmentItem(new LanScanner(),false, View.GONE);
    public static final NavigationItem SIGNAL_METER = new FragmentItem(new SignalMeterFragment2(),false,View.GONE);
    public static final NavigationItem PING = new FragmentItem(new PingFragment());
    public static final NavigationItem TRACEROUTE = new FragmentItem(new TraceRouteFragment(),false,View.GONE);
    public static final NavigationItem SPEEDTEST = new FragmentItem(new SpeedTestFragment(),false,View.GONE);
    public static final NavigationItem OPENPORTTOOL = new FragmentItem(new OpenPortTestFragment(), false,View.GONE);
    public static final NavigationItem DOMAINWHOIS = new FragmentItem(new DomainFragment(),false,View.GONE);
    public static final NavigationItem IPV4CALC = new FragmentItem(new IpV4Calculator(),false,View.GONE);
    public static final NavigationItem IPV4CONV = new FragmentItem(new Converter(),false,View.GONE);
    public static final NavigationItem DEVICE = new IntentItem();


    private NavigationItemFactory() {
        throw new IllegalStateException("Factory class");
    }
}
