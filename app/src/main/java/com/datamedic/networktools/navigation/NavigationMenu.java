/*
 * WiFiAnalyzer
 * Copyright (C) 2018  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
// "This file was modified by John Poulikidis in 2019."
package com.datamedic.networktools.navigation;

import androidx.annotation.NonNull;
import android.view.MenuItem;

import com.datamedic.networktools.MainActivity;
import com.datamedic.networktools.R;
import com.datamedic.networktools.navigation.availability.NavigationOption;
import com.datamedic.networktools.navigation.availability.NavigationOptionFactory;
import com.datamedic.networktools.navigation.items.NavigationItem;
import com.datamedic.networktools.navigation.items.NavigationItemFactory;

import org.apache.commons.collections4.Closure;
import org.apache.commons.collections4.IterableUtils;

import java.util.List;

public enum NavigationMenu {
    ACCESS_POINTS(R.drawable.ic_network_wifi_grey_500_48dp, R.string.action_access_points, NavigationItemFactory.ACCESS_POINTS, NavigationOptionFactory.AP),
    SIGNAL_METER(R.drawable.ic_info_outline_grey_500_48dp, R.string.action_signal_meter, NavigationItemFactory.SIGNAL_METER),
    CHANNEL_RATING(R.drawable.ic_wifi_tethering_grey_500_48dp, R.string.action_channel_rating, NavigationItemFactory.CHANNEL_RATING, NavigationOptionFactory.RATING),
    CHANNEL_GRAPH(R.drawable.ic_insert_chart_grey_500_48dp, R.string.action_channel_graph, NavigationItemFactory.CHANNEL_GRAPH, NavigationOptionFactory.OTHER),
    TIME_GRAPH(R.drawable.ic_show_chart_grey_500_48dp, R.string.action_time_graph, NavigationItemFactory.TIME_GRAPH, NavigationOptionFactory.OTHER),
    CHANNEL_AVAILABLE(R.drawable.ic_location_on_grey_500_48dp, R.string.action_channel_available, NavigationItemFactory.CHANNEL_AVAILABLE),
    LANSCANNER(R.drawable.ic_lan_black_24dp, R.string.action_lanscanner, NavigationItemFactory.LANSCANNER),
    SPEEDTEST(R.drawable.ic_spped_check_black_24dp, R.string.speed_test, NavigationItemFactory.SPEEDTEST),
    TRACEROUTE(R.drawable.ic_ping_black_24dp, R.string.traceroute, NavigationItemFactory.TRACEROUTE),
    OPENPORTTOOL(R.drawable.ic_open_port_tool_24dp, R.string.openporttest, NavigationItemFactory.OPENPORTTOOL),
    DOMAINWHOIS(R.drawable.ic_perm_identity_black_24dp, R.string.DomainWhois, NavigationItemFactory.DOMAINWHOIS),
    PING(R.drawable.ic_pingnew_black_24dp, R.string.pingtool, NavigationItemFactory.PING),
    IPV4CALC(R.drawable.ic_keyboard_black_24dp, R.string.ipv4calc, NavigationItemFactory.IPV4CALC),
    IPV4CONV(R.drawable.ic_memory_black_24dp, R.string.ipv4conv, NavigationItemFactory.IPV4CONV),
    DEVICE(R.drawable.ic_smartphone_black_24dp,R.string.device_info,NavigationItemFactory.DEVICE),
    EXPORT(R.drawable.ic_import_export_grey_500_48dp, R.string.action_export, NavigationItemFactory.EXPORT),
    VENDORS(R.drawable.ic_list_grey_500_48dp, R.string.action_vendors, NavigationItemFactory.VENDORS),
    SETTINGS(R.drawable.ic_settings_grey_500_48dp, R.string.action_settings, NavigationItemFactory.SETTINGS),
    ABOUT(R.drawable.ic_info_outline_grey_500_48dp, R.string.action_about, NavigationItemFactory.ABOUT);



    private final int icon;
    private final int title;
    private final List<NavigationOption> navigationOptions;
    private final NavigationItem navigationItem;

    NavigationMenu(int icon, int title, @NonNull NavigationItem navigationItem, @NonNull List<NavigationOption> navigationOptions) {
        this.icon = icon;
        this.title = title;
        this.navigationItem = navigationItem;
        this.navigationOptions = navigationOptions;
    }

    NavigationMenu(int icon, int title, @NonNull NavigationItem navigationItem) {
        this(icon, title, navigationItem, NavigationOptionFactory.MODULES);
    }

    public int getTitle() {
        return title;
    }

    public void activateNavigationMenu(@NonNull MainActivity mainActivity, @NonNull MenuItem menuItem) {
        navigationItem.activate(mainActivity, menuItem, this);
    }

    public void activateOptions(@NonNull MainActivity mainActivity) {
        IterableUtils.forEach(navigationOptions, new ActivateClosure(mainActivity));
    }

    public boolean isWiFiBandSwitchable() {
        return navigationOptions.contains(NavigationOptionFactory.WIFI_SWITCH_ON);
    }

    public boolean isRegistered() {
        return navigationItem.isRegistered();
    }

    int getIcon() {
        return icon;
    }

    @NonNull
    NavigationItem getNavigationItem() {
        return navigationItem;
    }

    @NonNull
    List<NavigationOption> getNavigationOptions() {
        return navigationOptions;
    }

    private class ActivateClosure implements Closure<NavigationOption> {
        private final MainActivity mainActivity;

        private ActivateClosure(@NonNull MainActivity mainActivity) {
            this.mainActivity = mainActivity;
        }

        @Override
        public void execute(NavigationOption input) {
            input.apply(mainActivity);
        }

    }


}