/*
 * Network Tools
 * Copyright (C) 2018  DataMedic Business IT Support <networktools@datamedic.gr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */


package com.datamedic.networktools.domainwhois;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.datamedic.networktools.R;

import org.apache.commons.net.whois.WhoisClient;

import java.io.IOException;
import java.util.Arrays;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class DomainFragment extends Fragment {

    EditText editTextDomain;
    Button checkdomain;
    TextView resulttext;
    protected ProgressDialog ProgressDialog;

    public String theWord = "";


    @Override
    public void onPause() {
        super.onPause();

        Thread.currentThread().interrupt();


    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.domainwhois, container, false);

        editTextDomain = view.findViewById(R.id.editTextDomain);
        checkdomain = view.findViewById(R.id.checkdomain);
        resulttext = view.findViewById(R.id.resulttext);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        checkdomain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (editTextDomain.getText().toString().isEmpty()) {
                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            Toast.makeText(getContext(), "Please type a domain name",
                                    Toast.LENGTH_SHORT).show();

                        }
                    });


                } else {

                    Thread t = new Thread() {
                        public void run() {
                            leveloneWhois();

                        }
                    };
                    t.start();
                    ProgressDialog = new ProgressDialog(getContext(), R.style.NewDialog);
                    ProgressDialog.setCancelable(false);
                    ProgressDialog.getWindow().setGravity(Gravity.CENTER);
                    ProgressDialog.setTitle("Gathering info.... Please wait");
                    ProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    ProgressDialog.show();



                }
            }
        });


        return view;
    }


    private void leveloneWhois() {


        final String WHOIS_SERVER = "whois.internic.net";
        final int WHOIS_PORT = 43;


        String nameToQuery = editTextDomain.getText().toString();

        try {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception keyerror) {
            keyerror.printStackTrace();
        }



        WhoisClient whoisClient = new WhoisClient();
        try {
            whoisClient.connect(WHOIS_SERVER, WHOIS_PORT);

        } catch (IOException e) {
            e.printStackTrace();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    ProgressDialog.cancel();


                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(getContext(), "No connection to Internet or unknown domain name",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            });


        }
        String results = null;

        if (whoisClient.isConnected() == false) {

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Toast.makeText(getContext(), "No connection to Internet or unknown domain name",
                            Toast.LENGTH_SHORT).show();
                }
            });





            return;
        } else {

            try {
                results = whoisClient.query(nameToQuery);
            } catch (IOException e) {
                e.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        ProgressDialog.cancel();
                    }
                });

            }
            Log.d("tophre", results);


            String words[] = results.split(" ");


            for (String word : words) {
                if (word.toLowerCase().startsWith("whois.")) {
                    theWord = word;


                    Log.d("tophre", theWord + "ok");


                }


            }


            System.out.println(results);


            Log.d("tophre2", results + "    ok");

            String[] lines = results.split(System.getProperty("line.separator"));

            for (int i = 0; i < lines.length; i++) {
                if (lines[i].startsWith("NOTICE:")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("registrar's sponsorship")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("currently set to")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("date of the domain")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("registrar.  Users")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("view the registrar's")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("TERMS OF USE")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("database through")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("automated except")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("modify existing")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("Services' (\"VeriSign\")")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("information purposes")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("about or related")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("guarantee its")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("by the following")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("for lawful purposes")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("to: (1)")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("unsolicited,")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("or facsimile; ")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("that apply to")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("repackaging, dissemination")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("prohibited without")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("use electronic")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("query the Whois")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("domain names or")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("to restrict your")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("operational stability")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("Whois database for")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("reserves the right")) {
                    lines[i] = "";
                }


            }
            StringBuilder finalStringBuilder = new StringBuilder("");
            for (String s : lines) {
                if (!s.equals("")) {
                    finalStringBuilder.append(s).append(System.getProperty("line.separator"));
                }
            }
            String finalString = finalStringBuilder.toString();


            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    resulttext.setText(finalString);
                    resulttext.setMovementMethod(new ScrollingMovementMethod());

                }
            });

            try {
                whoisClient.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        ProgressDialog.cancel();
                    }
                });
            }
        }
        if (results == null) {


            Toast.makeText(getContext(), "Unable to resolve domain or there is no internet connectivity",
                    Toast.LENGTH_SHORT).show();
            return;
        } else {
            levelTwoWhois();
        }
    }


    public void levelTwoWhois() {
        String newWord = theWord.trim();


        final String WHOIS_SERVER = newWord;
        final int WHOIS_PORT = 43;

        String nameToQuery = editTextDomain.getText().toString();

        WhoisClient whoisClient = new WhoisClient();
        try {
            whoisClient.connect(WHOIS_SERVER, WHOIS_PORT);

        } catch (IOException e) {
            e.printStackTrace();


            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    ProgressDialog.cancel();
                }
            });


        }


        String results = null;


        if (whoisClient.isConnected() == false) {


            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Toast.makeText(getContext(), "No connection to Internet or unknown domain name",
                            Toast.LENGTH_SHORT).show();
                }
            });


        } else {

            try {
                results = whoisClient.query(nameToQuery);
            } catch (IOException e) {
                e.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        ProgressDialog.cancel();
                    }
                });
            }


            System.out.println(results);


            String[] lines = results.split(System.getProperty("line.separator"));

            for (int i = 0; i < lines.length; i++) {
                if (lines[i].startsWith("NOTICE:")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("automated, electronic")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("The Data in the CSC WHOIS")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("and to assist persons")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("CSC does not")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("By submitting a WHOIS")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("you agree that")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("will you use this")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("commercial advertising")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("or facsimile; or")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith("CSC reserves the ")) {
                    lines[i] = "";
                }
                if (lines[i].startsWith(" CSC reserves the right")) {
                    lines[i] = "";
                }


            }
            Log.d("tophre3", "" + Arrays.toString(lines));
            StringBuilder finalStringBuilder = new StringBuilder("");
            for (String s : lines) {
                if (!s.equals("")) {
                    finalStringBuilder.append(s).append(System.getProperty("line.separator"));
                }
            }
            String finalString = finalStringBuilder.toString();


            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    resulttext.append(finalString);
                    resulttext.setMovementMethod(new ScrollingMovementMethod());


                    ProgressDialog.cancel();
                }
            });





        }

    }

    public String removeLines(String word, String remove) {


        return word.replace(word, remove);

    }
}









