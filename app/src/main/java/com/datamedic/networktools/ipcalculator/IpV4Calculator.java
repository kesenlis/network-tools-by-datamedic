/*
 * Network Tools
 * Copyright (C) 2018  DataMedic Business IT Support <networktools@datamedic.gr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */


package com.datamedic.networktools.ipcalculator;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;


import com.datamedic.networktools.MainActivity;
import com.datamedic.networktools.R;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import static android.app.Activity.RESULT_OK;


public class IpV4Calculator extends Fragment {


    private static final String TAG = IpV4Calculator.class.getSimpleName();
    private static final boolean debug = false;

    private TextView msgIPAddress;
    private String CurrentIP;
    private int CurrentBits;
    Animation anim = null;
    MainActivity mainActivity;


    public static final int REQUEST_HISTORY = 0;
    public static final int REQUEST_CONVERT = 1;

    LinearLayout outerLayout;





        @Override
        public void onPause() {
            super.onPause();




    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ipv4calc, container, false);

        if (debug) Log.d(TAG, "onCreate()");

        anim = AnimationUtils.loadAnimation(getContext(), R.anim.highlight);




        outerLayout = view.findViewById(R.id.main_outer_layout);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);


        SharedPreferences settings = getActivity().getPreferences(0);
        CurrentIP = settings.getString("CurrentIP", "");
        CurrentBits = settings.getInt("CurrentBits", 24);

        Spinner s1 = view.findViewById(R.id.bitlength);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                getContext(), R.array.bitlengths, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s1.setAdapter(adapter);

        Spinner s2 = view.findViewById(R.id.subnetmask);
        ArrayAdapter<CharSequence> subnetmask_adapter = ArrayAdapter.createFromResource(
                getContext(), R.array.subnets, android.R.layout.simple_spinner_item);
        subnetmask_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s2.setAdapter(subnetmask_adapter);

        s1.setOnItemSelectedListener(mBitlengthSelectedListener);
        s2.setOnItemSelectedListener(mSubnetMaskSelectedListener);
        s1.setSelection(CurrentBits - 1);
        s2.setSelection(CurrentBits - 1);

        Button calculate = view.findViewById(R.id.calculate);
        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager imm = (InputMethodManager) getActivity()
                        .getSystemService(getContext().INPUT_METHOD_SERVICE);


                if (imm.isAcceptingText()) {

                    ((InputMethodManager) getContext().getSystemService(Activity.INPUT_METHOD_SERVICE))
                            .toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);

                }



                doCalculate();

            }
        });

        Button reset =  view.findViewById(R.id.reset);

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity()
                        .getSystemService(getContext().INPUT_METHOD_SERVICE);


                if (imm.isAcceptingText()) {

                    ((InputMethodManager) getContext().getSystemService(Activity.INPUT_METHOD_SERVICE))
                            .toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);

                }


                CurrentIP = "";
                CurrentBits = 24;

                updateFields();

                ClearResults();
            }
        });

        msgIPAddress = view.findViewById(R.id.ipaddress);
        if (!CurrentIP.equals("")) {
            msgIPAddress.setText(CurrentIP);

        }
        return view;
    }



    @Override
    public void onStop() {
        super.onStop();

        if (debug) Log.d(TAG, "onStop()");
        updateResults(false);
        SharedPreferences settings = getActivity().getPreferences(0);
        SharedPreferences.Editor editor = settings.edit();
        if (debug) Log.d(TAG, "storing CurrentIP=" + CurrentIP);
        editor.putString("CurrentIP", CurrentIP);
        editor.putInt("CurrentBits", CurrentBits);

        editor.apply();
    }



    private String IntIPToString(int in) {
        int quad1 = ((in & 0xFF000000) >> 24) & 0xFF;
        int quad2 = (in & 0x00FF0000) >> 16;
        int quad3 = (in & 0x0000FF00) >> 8;
        int quad4 = (in & 0x000000FF);

        return String.format("%d.%d.%d.%d", quad1, quad2, quad3, quad4);
    }


    private void UpdateSubnetmaskFromBitlength() {
        Spinner bitlength_spinner =  getView().findViewById(R.id.bitlength);
        Spinner subnetmask_spinner = getView().findViewById(R.id.subnetmask);

        subnetmask_spinner.setSelection(bitlength_spinner.getSelectedItemPosition());
    }


    private void UpdateBitlengthFromSubnetmask() {
        Spinner bitlength_spinner =  getView().findViewById(R.id.bitlength);
        Spinner subnetmask_spinner =  getView().findViewById(R.id.subnetmask);

        bitlength_spinner.setSelection(subnetmask_spinner.getSelectedItemPosition());
    }

    private AdapterView.OnItemSelectedListener mBitlengthSelectedListener = new AdapterView.OnItemSelectedListener() {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            UpdateSubnetmaskFromBitlength();
            updateResults(true);
        }

        public void onNothingSelected(AdapterView<?> parent) {
            UpdateSubnetmaskFromBitlength();
        }

    };

    private AdapterView.OnItemSelectedListener mSubnetMaskSelectedListener = new AdapterView.OnItemSelectedListener() {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            UpdateBitlengthFromSubnetmask();
            updateResults(true);
        }

        public void onNothingSelected(AdapterView<?> parent) {
            UpdateBitlengthFromSubnetmask();
        }

    };


    private void ClearResults() {
        TextView msgAddressRange =  getView().findViewById(R.id.address_range);
        TextView msgMaximumAddresses =  getView().findViewById(R.id.maximum_addresses);
        TextView msgWildcard =  getView().findViewById(R.id.wildcard);
        TextView msgIPBinaryNetwork =  getView().findViewById(R.id.ip_binary_network);
        TextView msgIPBinaryHost =  getView().findViewById(R.id.ip_binary_host);
        TextView msgIPBinaryNetmask =  getView().findViewById(R.id.ip_binary_netmask);

        msgAddressRange.setText("");
        msgMaximumAddresses.setText("");
        msgWildcard.setText("");
        msgIPBinaryNetwork.setText("");
        msgIPBinaryHost.setText("");
        msgIPBinaryNetmask.setText("");
    }

    public static int stringIPtoInt(String ip) throws Exception {
        String[] quad = ip.split("\\.", 4);
        if (quad.length != 4) {
            throw new Exception();
        }
        int ip32bit = 0;
        for (String value : quad) {
            if (value.length() < 1) {
                throw new Exception();
            }
            int octet;
            try {
                octet = Integer.parseInt(value);
            } catch (NumberFormatException e) {
                throw new Exception();
            }

            if (octet > 255) {
                throw new Exception();
            }
            ip32bit = ip32bit << 8;
            ip32bit = ip32bit | octet;
        }
        return ip32bit;
    }


    private boolean updateResults(boolean updateView) {
        TextView msgAddressRange =  getView().findViewById(R.id.address_range);
        TextView msgMaximumAddresses =  getView().findViewById(R.id.maximum_addresses);
        TextView msgWildcard =  getView().findViewById(R.id.wildcard);
        TextView msgIPBinaryNetwork =  getView().findViewById(R.id.ip_binary_network);
        TextView msgIPBinaryHost =  getView().findViewById(R.id.ip_binary_host);
        TextView msgIPBinaryNetmask =  getView().findViewById(R.id.ip_binary_netmask);
        Spinner bitlength_spinner =  getView().findViewById(R.id.bitlength);
        Spinner subnetmask_spinner =  getView().findViewById(R.id.subnetmask);

        CharSequence ipAddressText = msgIPAddress.getText();
        if (ipAddressText == null) {
            return false;
        }
        String ip = ipAddressText.toString();
        int ip32bit;
        try {
            ip32bit = stringIPtoInt(ip);
        } catch (Exception e) {
            ClearResults();
            return false;
        }
        String selectedItem = (String) bitlength_spinner.getSelectedItem();
        if (selectedItem == null) {
            return false;
        }
        int bitlength = Integer.parseInt(
                selectedItem.substring(1));
        if (debug) Log.d(TAG, "bitlength=" + bitlength);

        int ip32bitmask = (1 << (32 - bitlength)) - 1;

        int firstip = ip32bit & (~ip32bitmask);
        int lastip = firstip | ip32bitmask;

        String ipFirst = IntIPToString(firstip);
        String ipLast = IntIPToString(lastip);

        int maximumAddresses;
        if (ip32bitmask > 0) {
            maximumAddresses = ip32bitmask - 1;
        } else {
            maximumAddresses = 0;
        }

        String wildcard = IntIPToString(ip32bitmask);
        String binary = Converter.convertIPIntDec2StringBinary(ip32bit);
        int netmask = (~ip32bitmask);
        String binaryNetmask = Converter.convertIPIntDec2StringBinary(netmask);

        CurrentIP = ip;
        CurrentBits = bitlength;

        if (updateView) {
            String addressRange = ipFirst + " - " + ipLast;
            msgAddressRange.setText(addressRange);
            String maximumAddressesString = String.format("%d", maximumAddresses);
            msgMaximumAddresses.setText(maximumAddressesString);
            msgWildcard.setText(wildcard);

            int networkHostCutoff;
            if (bitlength >= 24) {
                networkHostCutoff = bitlength + 3;
            } else if (bitlength >= 16) {
                networkHostCutoff = bitlength + 2;
            } else if (bitlength >= 8) {
                networkHostCutoff = bitlength + 1;
            } else {
                networkHostCutoff = bitlength;
            }
            String binary_network = binary.substring(0, networkHostCutoff);
            String binary_host = binary.substring(networkHostCutoff);
            msgIPBinaryNetwork.setText(binary_network);
            msgIPBinaryHost.setText(binary_host);
            msgIPBinaryNetmask.setText(binaryNetmask);

            msgAddressRange.startAnimation(anim);
            String subnetmask = (String) subnetmask_spinner.getSelectedItem();

            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());

        }

        return true;
    }


    private DialogInterface.OnClickListener mCalculateListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            doCalculate();
        }


    };

    private void doCalculate() {
        if (!updateResults(true)) {
            TextView msgAddressRange =  getView().findViewById(R.id.address_range);

            msgAddressRange.setText(R.string.err_bad_ip);
        }
    }

    private DialogInterface.OnClickListener mResetListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            CurrentIP = "";
            CurrentBits = 24;

            updateFields();

            ClearResults();
        }


    };

    private void updateFields() {
        msgIPAddress.setText(CurrentIP);

        Spinner bitlength_spinner =  getView().findViewById(R.id.bitlength);
        bitlength_spinner.setSelection(CurrentBits - 1);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_HISTORY) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                if (debug) Log.d(TAG, "got: " + uri);
                if (uri == null) {
                    return;
                }


                updateFields();
                updateResults(true);

            }
        } else if (requestCode == REQUEST_CONVERT) {
            if (resultCode == RESULT_OK) {
                CurrentIP = data.getStringExtra(Converter.EXTRA_IP);
                msgIPAddress.setText(CurrentIP);
            }
        }

    }


}









