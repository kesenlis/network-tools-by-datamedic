<h1> Network Tools By DataMedic</h1>


This is the official repository of NetWork Tools.

Network Tools is based on several Open source Programs and our own code in an effort to make an all in out network tools program for android.



[<img src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png" alt="Get it on Google Play" height="80">](https://play.google.com/store/apps/details?id=com.datamedic.networktools)


WiFi Analizer by VREM Software
https://github.com/VREMSoftwareDevelopment/WiFiAnalyzer

Lan Scanner is based on Port Authority by Aaron J Wood
https://github.com/aaronjwood/PortAuthority

SpeedTest by egcodes 
https://github.com/egcodes/speed-test

Trace route is Bases on Traceroute Ping by Olivier Goutay
https://github.com/olivierg13/TraceroutePing

Notice : All the open source programs have been altered in one way or another.



Features

Maintain or check your WiFi and LAN network using Network Tools by examining surrounding WiFi networks, measuring their signal strength as well as identifying crowded channels and take internet speed measurements.


Network tools is not a WiFi password cracking or phishing tool.

Features:
- Limited Ads : Only 1 advertisment per Hour
- Dedicated Signal Meter - Wifi Scanner
- Identify nearby Access Points
- Graph channels signal strength
- Graph Access Point signal strength over time
- Analyze WiFi networks to rate channels
- HT/VHT Detection - 40/80/160MHz (Requires Android OS 6+)
- Access Point view complete or compact
- Estimated Distance to the Access Points
- Export access points details
- Pause/Resume scanning
- Available filters: WiFi band, Signal strength, Security and SSID
- Vendor/OUI Database Lookup
- LAN Scanner 
- Internet Speed Test*
-Trace Route
- Ping Tool
- Port Scanner


Usage Tips:
- Tap the title bar to switch between 2.4 and 5 GHz WiFi band.
- Swipe to manually refresh screen content.
- Swipe left/right at the bottom of the screen to navigate to the next/previous screen
- SSID with (***) means it is hidden.


*on active development.


Supported Languages

Chinese
French
German
Italian
Polish
Portuguese
Russian
Spanish
Greek

Enjoy having a well managed WiFi and LAN network using Network Tools by examining surrounding WiFi networks, measuring their signal strength as well as identifying crowded channels and take internet speed measurements.********


## License
[<img src="https://www.gnu.org/graphics/gplv3-127x51.png" alt="GPLv3" >](http://www.gnu.org/licenses/gpl-3.0.html)


Network Tools  is licensed under the GNU General Public License v3.0 (GPLv3).

### GPLv3 License key requirements:
* Disclose Source
* License and Copyright Notice
* Same License
* State Changes

[GNU General Public License v3.0 (GPLv3) Explained in Plain English](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3))

[GNU General Public License v3.0 (GPLv3)](http://www.gnu.org/licenses/gpl-3.0.html).



**Build project**

Install Android Studio

Import project into Android Studio

In Welcome to Android Studio Screen select Import project ...

Select the root directory of the WiFi Analyzer repository and click "OK".

Network tools  will build automatically.

In case of achart engine error being corrupted please download the file from

[Achart Engine Download](https://github.com/JasonZhangHkust/BLEJumpHeight/tree/master/android/achartengine-1.2.0)

Copy and paste it on project/app/libs/

Rebuild Project

